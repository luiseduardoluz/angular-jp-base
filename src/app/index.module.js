(function ()
{
    'use strict';

    /**
     * Main module of the app
     */
    angular
        .module('app', [

            // Common 3rd Party Dependencies
            'uiGmapgoogle-maps',
            'textAngular',
            'xeditable',

            // core
            'ngAnimate',
            'ngAria',
            'ngCookies',
            'ngMessages',
            'ngResource',
            'ngSanitize',            
            'ui.router',
            'pascalprecht.translate',

            // Lean
            'app.lean'
        ]);
})();
