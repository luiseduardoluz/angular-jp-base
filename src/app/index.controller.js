(function ()
{
    'use strict';

    angular
        .module('app')
        .controller('IndexController', IndexController);

    /** @ngInject */
    function IndexController(APP_NAME, APP_VERSION, APP_AUTHOR)
    {
        var vm = this;

        // Data
        console.log(APP_NAME + ' v' + APP_VERSION + ' by ' + APP_AUTHOR);
        console.log("Its Work! ;)");
        //////////
    }
})();