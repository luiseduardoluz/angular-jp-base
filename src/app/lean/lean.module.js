(function ()
{
    'use strict';

    angular
        .module('app.lean',
            [
                'datatables',
                'flow'
            ]
        )
        .config(config);

    /** @ngInject */
    function config($stateProvider, $translatePartialLoaderProvider)
    {
        $stateProvider
            .state('app.lean', {
                url      : '/lean',
                views    : {
                    'content@app': {
                        templateUrl: '/app/lean/views/index.html',
                        controller : 'LeanController as vm'
                    }
                },
                resolve  : {
                },
                bodyClass: 'lean'
            });
                 

        // Api

        // Translation
        $translatePartialLoaderProvider.addPart('app/lean');


    }

})();