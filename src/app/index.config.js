(function ()
{
    'use strict';

    angular
        .module('app')
        .config(config);

        /** @ngInject */
    function config(APP_LANGUAGE,uiGmapGoogleMapApiProvider, $translateProvider, $provide)
    {
        // Put your common app configurations here

        // angular-translate configuration
        $translateProvider.useLoader('$translatePartialLoader', {
            urlTemplate: '{part}/i18n/{lang}.json'
        });
        $translateProvider.preferredLanguage(APP_LANGUAGE);
        $translateProvider.useSanitizeValueStrategy('sanitize');

    }

})();