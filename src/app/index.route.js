(function ()
{
    'use strict';

    angular
        .module('app')
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider, $locationProvider)
    {
        $locationProvider.html5Mode(true);

        $urlRouterProvider.otherwise('/lean');
        // State definitions
        $stateProvider
            .state('app', {
                abstract: true,
                views   : {
                    'main@': {
                        templateUrl: "/app/lean/views/index.html",
                        controller : 'LeanController as vm'
                    }
                }
            });
    }

})();