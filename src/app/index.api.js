(function ()
{
    'use strict';

    angular
        .module('app')
        .provider('api', apiService);

    /** @ngInject */
    function apiService()
    {
        var settings = {};
        var utils = {};

        settings.clientId = null;
        settings.redirectUri = null;
        settings.scope = null;
        settings.authToken = null;
        settings.typeUser = null;
        settings.urlOauth = null;

        utils.toQueryString = function (obj)
        {
           var parts = [];
           angular.forEach(obj, function (value, key)
           {
               this.push(encodeURIComponent(key) + '=' + encodeURIComponent(value));
           }, parts);
           return parts.join('&');
        };

        this.$get = ['$q', '$http', '$location', function ($q, $http, $location)
        {

            function LeanApi ()
            {
                this.clientId = settings.clientId;
                this.redirectUri = settings.redirectUri;
                this.apiBase = settings.apiBase;
                this.scope = settings.scope;
                this.authToken = settings.authToken;
                this.toQueryString = utils.toQueryString;
             }

            function openDialog (uri, name, options, cb)
            {
                var win = window.open(uri, name, options);
                var interval = window.setInterval(function () {
                try {
                    if (!win || win.closed) {
                        window.clearInterval(interval);
                        cb(win);
                    }
                } catch (e) {}
                }, 1000);
                return win;
            }

            if ($location.host() == "localhost") {
                settings.apiBase = 'http://localhost:3000/api/public';
                settings.urlOauth = 'http://localhost:8100/callback.html';
            } else {
                // Production
                //settings.apiBase = 'http://apirais.cloudapp.net/v1';

                // Localhost
                settings.apiBase = 'http://localhost:3000/api/public';

                // Dev/Prod
                // settings.apiBase = 'http://' + $location.host() + '/v1';
                settings.urlOauth = "http://localhost/callback";
            }


            LeanApi.prototype =  {

                api: function (endpoint, method, params, data, headers)
                {
                    var deferred = $q.defer();

                    $http({
                        url: this.apiBase + endpoint,
                        method: method ? method : 'GET',
                        params: params,
                        data: data,
                        headers: headers,
                        withCredentials: false
                    })
                    .then(
                        function successCallback(response)
                        {
                            deferred.resolve(response.data);
                        }, 
                        function errorCallback(response)
                        {
                            deferred.reject(response.data);
                        }
                    );
                    return deferred.promise;
                },

                _auth: function (isJson)
                {
                    var token = localStorage.getItem("accessToken");
                    var auth = {
                        'Authorization': token
                    };
                    if (isJson) {
                        auth['Content-Type'] = 'application/json';
                    }
                    return auth;
                },


                /**
                 * Login REST functions - GET
                 */
                getToken: function (options, data)
                {
                    return this.api('/authenticate', 'POST', options, data, false);
                },
                getRestricted: function ()
                {
                    return this.api('/restricted', 'GET', null, null, this._auth(true));
                },

                /**
                 * User REST functions - GET
                 */
                getUser: function (id)
                {
                    return this.api('/user/'+id, 'GET', null, null, this._auth(true));
                },
                getFullUser: function (id)
                {
                    return this.api('/user/full/'+id, 'GET', null, null, this._auth(true));
                },
                getClients: function (id)
                {
                    return this.api('/user/clients/', 'GET', null, null, this._auth(true));
                },
                saveUser: function (data)
                {
                    return this.api('/user/add', 'POST', null, data, this._auth(true));
                },
                // updateUser: function(type, options, data)
                // {
                //     return this.api('/user', 'PUT', options, data, this._auth(true));
                // },

                /**
                 * Obras REST functions - GET, POST, PUT
                */
                getObras: function ()
                {
                    return this.api('/obras', 'GET', null, null, this._auth(true));
                },
                saveObra: function (data)
                {
                    return this.api('/obras/add', 'POST', null, data, this._auth(true));
                },

                /**
                 * Materias Obra REST functions - GET, POST, PUT
                 */
                getMateriais: function ()
                {
                    return this.api('/materiais', 'GET', null, null, this._auth(true));
                },
                saveMaterial: function (data)
                {
                    return this.api('/materiais/add', 'POST', null, data, this._auth(true));
                },
                /**
                 * Materias Obra REST functions - GET, POST, PUT
                 */
                getMateriasObra: function (id_obra)
                {
                    return this.api('/materiais-obra/obra/'+id_obra, 'GET', null, null, this._auth(true));
                },
                saveMateriasObra: function (data)
                {
                    return this.api('/materiais-obra/add', 'POST', null, data, this._auth(true));
                }

            };  

            return new LeanApi();
        }];
    }

})();