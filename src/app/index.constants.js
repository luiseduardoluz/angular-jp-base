(function ()
{
    'use strict';

    angular
        .module('app')
        .constant('APP_AUTHOR', 'Luis Eduardo Ferraz Luz')
        .constant('APP_NAME', 'AngularJS Template')
        .constant('APP_VERSION', '0.0.1')
        .constant('APP_LANGUAGE','br');

})();
